package reggol

import (
	"fmt"
	"strings"
)

const (
	// ColorStop is the ansi code to terminate colorizing output.
	ColorStop = "\033[0m"
)

const (
	klogMessageMinimumParts = 5
	maxNameLen              = 25
	maxNameLenNoColor       = 32
)

// LogLevelColorMap is the mapping of log levels (debug/info/warn/critical) to their respective
// color ansi color code (blue/green/yellow/red respectively).
var LogLevelColorMap = map[LogLevel]string{ //nolint:gochecknoglobals
	Debug:    "\033[34m",
	Info:     "\033[32m",
	Warn:     "\033[33m",
	Critical: "\033[31m",
}

// Formatter is a type representing a valid logging formatter function. It should accept a logging
// instance, a level string, and a message string, returning a formatted message string.
type Formatter func(i Instance, l LogLevel, m string) string

func shortenString(s string, maxLen int) string {
	if len(s) <= maxLen {
		return s
	}

	return fmt.Sprintf("%s*", s[:maxLen-1])
}

// DefaultFormatter is the default logging instance formatter -- this formatter simply adds colors
// to the log message based on log level.
func DefaultFormatter(i Instance, l LogLevel, m string) string {
	var lr string

	switch l {
	case Debug:
		lr = LogLevelColorMap[Debug] + strings.ToUpper(string(l)) + ColorStop
	case Info:
		lr = LogLevelColorMap[Info] + strings.ToUpper(string(l)) + ColorStop
	case Warn:
		lr = LogLevelColorMap[Warn] + strings.ToUpper(string(l)) + ColorStop
	case Critical, Fatal:
		lr = LogLevelColorMap[Critical] + strings.ToUpper(string(l)) + ColorStop
	case Disabled:
		// should never get to here, but we can return an empty string if it does happen
		return ""
	}

	// pad the level extra for the ansi code magic
	return fmt.Sprintf("%17s | %25s | %s", lr, shortenString(i.GetName(), maxNameLen), m)
}

// DefaultNoColorFormatter is the default no color logging instance formatter.
func DefaultNoColorFormatter(i Instance, l LogLevel, m string) string {
	return fmt.Sprintf("%10s | %25s | %s", l, shortenString(i.GetName(), maxNameLenNoColor), m)
}

// NoopFormatter is a "no-op" formatter, it simply returns the message.
func NoopFormatter(_ Instance, _ LogLevel, m string) string {
	return m
}

// DefaultKlogFormatter is the default logging instance formatter for *klog* logs. Ortica redirects
// klog logs through its own logging manager to give us more control, this formatter is applied by
// default to those messages only.
func DefaultKlogFormatter(i Instance, l LogLevel, m string) string {
	_ = l

	parts := strings.Fields(m)

	if len(parts) < klogMessageMinimumParts {
		// obviously nicer at some point, just not sure what other styles/formats klog may
		// output at this point.
		Panic(fmt.Sprintf("failed parsing '%s'", m))
	}

	newM := fmt.Sprintf(
		"klog %s: %s",
		strings.TrimRight(parts[3], "]"),
		strings.Join(parts[4:], " "),
	)

	return DefaultFormatter(i, Info, newM)
}
