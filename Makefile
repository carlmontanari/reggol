.DEFAULT_GOAL := help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

fmt: ## Run formatters
	gofumpt -w .
	gci write --skip-generated .
	golines -w .

lint: fmt ## Run linters
	golangci-lint run

install-tools: ## Install lint/test tools
	go install mvdan.cc/gofumpt@latest
	go install github.com/daixiang0/gci@latest@latest
	go install github.com/segmentio/golines@latest
	go install gotest.tools/gotestsum@latest

install-code-generators: ## Install latest code-generator tools
	go install k8s.io/code-generator/cmd/deepcopy-gen@latest
	go install k8s.io/code-generator/cmd/openapi-gen@latest
	go install k8s.io/code-generator/cmd/client-gen@latest
	go install sigs.k8s.io/controller-tools/cmd/controller-gen@latest

run-deepcopy-gen: ## Run deepcopy-gen
	GOMOD111=on \
	deepcopy-gen \
	--go-header-file hack/boilerplate.go.txt \
	--input-dirs gitlab.com/fioriera/ortica/apis/... \
	--output-file-base zz_generated.deepcopy \
	--trim-path-prefix ${GOPATH}/src/gitlab.com/fioriera/ortica

run-openapi-gen: ## Run openapi-gen
	GOMOD111=on \
	openapi-gen \
	--go-header-file hack/boilerplate.go.txt \
	--input-dirs gitlab.com/fioriera/ortica/apis/... \
	--trim-path-prefix ${GOPATH}/src/gitlab.com/fioriera/ortica/clabernetes \
	--output-package gitlab.com/fioriera/ortica/generated/openapi

run-client-gen: ## Run client-gen
	GOMOD111=on \
	client-gen \
	--go-header-file hack/boilerplate.go.txt \
	--input-base gitlab.com/fioriera/ortica \
	--input apis/v1alpha1 \
	--trim-path-prefix ${GOPATH}/src/gitlab.com/fioriera/ortica \
	--output-package gitlab.com/fioriera/ortica/generated \
	--clientset-name clientset

run-generate-crds: ## Run controller-gen for crds
	controller-gen crd paths=./apis/... output:crd:dir=./assets/crds/

run-generate: install-code-generators run-deepcopy-gen run-openapi-gen run-client-gen run-generate-crds fmt ## Run all code gen tasks
