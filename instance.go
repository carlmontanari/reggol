package reggol

import (
	"fmt"
	"sync"
)

// Instance is a logging instance managed by the Manager.
type Instance interface {
	Debug(f string)
	Debugf(f string, a ...interface{})
	Info(f string)
	Infof(f string, a ...interface{})
	Warn(f string)
	Warnf(f string, a ...interface{})
	Critical(f string)
	Criticalf(f string, a ...interface{})
	Fatal(f string)
	Fatalf(f string, a ...interface{})
	// Write implements io.Writer so that an instance can be used most places. Messages received
	// via Write will always have the current formatter applied, and all messages will be queued
	// for egress unless this logging instance's level is Disabled.
	Write(p []byte) (n int, err error)
	GetName() string
	GetLevel() LogLevel
}

type instance struct {
	lock              sync.Mutex
	name              string
	level             LogLevel
	formatter         Formatter
	ioWriterFormatter Formatter
	c                 chan string
	done              chan interface{}
}

func (i *instance) GetName() string {
	return i.name
}

func (i *instance) GetLevel() LogLevel {
	return i.level
}

func (i *instance) enqueue(m string) {
	i.c <- m
}

func (i *instance) shouldLog(l LogLevel) bool {
	switch i.level {
	case Disabled:
		return false
	case Debug:
		return true
	case Info:
		switch l { //nolint:exhaustive
		case Info, Warn, Critical:
			return true
		default:
			return false
		}
	case Warn:
		switch l { //nolint:exhaustive
		case Warn, Critical:
			return true
		default:
			return false
		}
	case Critical:
		if l == Critical {
			return true
		}
	case Fatal:
		// shouldnt get here but, just in case lets log it
		return true
	}

	return false
}

// Debug accepts a Debug level log message with no formatting.
func (i *instance) Debug(f string) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Debug) {
		return
	}

	i.enqueue(i.formatter(i, Debug, f))
}

// Debugf accepts a Debug level log message normal fmt.Sprintf type formatting.
func (i *instance) Debugf(f string, a ...interface{}) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Debug) {
		return
	}

	i.enqueue(i.formatter(i, Debug, fmt.Sprintf(f, a...)))
}

// Info accepts an Info level log message with no formatting.
func (i *instance) Info(f string) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Info) {
		return
	}

	i.enqueue(i.formatter(i, Info, f))
}

// Infof accepts an Info level log message normal fmt.Sprintf type formatting.
func (i *instance) Infof(f string, a ...interface{}) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Info) {
		return
	}

	i.enqueue(i.formatter(i, Info, fmt.Sprintf(f, a...)))
}

// Warn accepts a Warn level log message with no formatting.
func (i *instance) Warn(f string) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Warn) {
		return
	}

	i.enqueue(i.formatter(i, Warn, f))
}

// Warnf accepts a Warn level log message normal fmt.Sprintf type formatting.
func (i *instance) Warnf(f string, a ...interface{}) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Warn) {
		return
	}

	i.enqueue(i.formatter(i, Warn, fmt.Sprintf(f, a...)))
}

// Critical accepts a Critical level log message with no formatting.
func (i *instance) Critical(f string) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Critical) {
		return
	}

	i.enqueue(i.formatter(i, Critical, f))
}

// Criticalf accepts a Critical level log message normal fmt.Sprintf type formatting.
func (i *instance) Criticalf(f string, a ...interface{}) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if !i.shouldLog(Critical) {
		return
	}

	i.enqueue(i.formatter(i, Critical, fmt.Sprintf(f, a...)))
}

// Fatal accepts a Fatal level log message with no formatting. After emitting the message the log
// manager is flushed and the program is crashed via the calbernetesutil.Panic function.
func (i *instance) Fatal(f string) {
	i.lock.Lock()
	defer i.lock.Unlock()

	formattedMsg := i.formatter(i, Fatal, f)

	i.enqueue(formattedMsg)

	GetManager().Flush()

	_panic(formattedMsg)
}

// Fatalf accepts a Fatal level log message normal fmt.Sprintf type formatting. After emitting the
// message the log manager is flushed and the program is crashed via the calbernetesutil.Panic
// function.
func (i *instance) Fatalf(f string, a ...interface{}) {
	i.lock.Lock()
	defer i.lock.Unlock()

	formattedMsg := i.formatter(i, Fatal, fmt.Sprintf(f, a...))

	i.enqueue(formattedMsg)

	GetManager().Flush()

	_panic(formattedMsg)
}

// Write allows a logging instance to be used as an io.Writer. Messages received via this method
// will always be logged at informational unless the logger level is Disabled.
func (i *instance) Write(p []byte) (n int, err error) {
	i.lock.Lock()
	defer i.lock.Unlock()

	if i.level == Disabled {
		return 0, nil
	}

	if i.ioWriterFormatter != nil {
		i.enqueue(i.ioWriterFormatter(i, Info, string(p)))
	} else {
		i.enqueue(i.formatter(i, Info, string(p)))
	}

	return len(p), nil
}
