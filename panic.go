package reggol

import (
	"syscall"
	"time"
)

// Panic tries to panic "nicely" but will send a second sigint to really kill the process if the
// first does not succeed within a second, and if for some reason that one does not kill it, we try
// a sigkill, and finally if that still didn't work, we will simply panic outright.
func Panic(msg string) {
	done := make(chan struct{})

	go func() {
		defer close(done)

		if managerInstance == nil {
			return
		}

		managerInstance.panic(msg)

		GetManager().Flush()
	}()

	_panic(msg)
}

// _panic is used by instances when they get a fatal log message. this bypasses the "panic" logger
// instance since we dont want that to be triggered unless users are calling Panic directly outside
//
//	a logging instance.
func _panic(msg string) {
	pid := syscall.Getpid()

	err := syscall.Kill(pid, syscall.SIGINT)
	if err != nil {
		panic(msg)
	}

	time.Sleep(time.Second)

	err = syscall.Kill(pid, syscall.SIGINT)
	if err != nil {
		panic(msg)
	}

	time.Sleep(time.Second)

	err = syscall.Kill(pid, syscall.SIGKILL)
	if err != nil {
		panic(msg)
	}

	panic(msg)
}
