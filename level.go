package reggol

// LogLevel is an enum(ish) for log levels.
type LogLevel string

const (
	// Debug is the debug log level.
	Debug LogLevel = "debug"
	// Info is the info(rmational) log level.
	Info LogLevel = "info"
	// Warn is the warning log level.
	Warn LogLevel = "warn"
	// Critical is the critical log level.
	Critical LogLevel = "critical"
	// Fatal is the fatal log level.
	Fatal LogLevel = "fatal"
	// Disabled is the disabled (no logging) log level.
	Disabled LogLevel = "disabled"

	// DefaultLogLevel is the default logging level.
	DefaultLogLevel = Debug
)
