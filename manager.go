package reggol

import (
	"context"
	"fmt"
	"sync"
	"time"
)

var (
	managerInstance     Manager   //nolint:gochecknoglobals
	managerInstanceOnce sync.Once //nolint:gochecknoglobals
)

const (
	flushWaitAttempts = 5
	flushWaitSleep    = 25 * time.Millisecond
	panicInstance     = "panic"
)

// InitManager initializes the logging manager with the provided options. It does nothing if the
// manager has already been initialized.
func InitManager(options ...Option) {
	managerInstanceOnce.Do(func() {
		m := &manager{
			formatter:       DefaultFormatter,
			instancesLock:   sync.Mutex{},
			instances:       map[string]*instance{},
			instanceCancels: map[string]context.CancelFunc{},
			loggers:         []func(...any){},
		}

		for _, option := range options {
			option(m)
		}

		if len(m.loggers) == 0 {
			m.loggers = []func(...interface{}){printLog}
		}

		m.instances[panicInstance] = &instance{
			lock:      sync.Mutex{},
			name:      panicInstance,
			level:     Fatal,
			formatter: m.formatter,
			c:         make(chan string),
			done:      make(chan interface{}),
		}

		managerInstance = m
	})
}

// GetManager returns the global logging Manager. Panics if the logging manager has *not* been
// initialized (via InitManager).
func GetManager() Manager {
	if managerInstance == nil {
		Panic(
			"Logging Manager instance is nil, 'GetManager' should never be called until the " +
				"manager process has been started",
		)
	}

	return managerInstance
}

// Manager is the interface representing the global logging manager singleton, this interface
// defines the ways to interact with this object.
type Manager interface {
	SetLoggerFormatter(name string, formatter Formatter) error
	SetLoggerIOWriterFormatter(name string, formatter Formatter) error
	SetLoggerFormatterAllInstances(formatter Formatter)
	SetLoggerLevelAllInstances(level LogLevel)
	SetLoggerLevel(name string, level LogLevel) error
	RegisterLogger(name string, level LogLevel) error
	RegisterAndGetLogger(name string, level LogLevel) (Instance, error)
	MustRegisterAndGetLogger(name string, level LogLevel) Instance
	GetLogger(name string) (Instance, error)
	DeleteLogger(name string)
	Flush()
	panic(f string)
}

// Manager is a "global" logging object for a clabernetes instance. It contains logging instances
// for individual components (controllers primarily), allowing logging at different levels for each
// component, but with a unified interface and message formatting system.
type manager struct {
	formatter       Formatter
	instancesLock   sync.Mutex
	instances       map[string]*instance
	instanceCancels map[string]context.CancelFunc
	loggers         []func(...interface{})
}

func (m *manager) start(i *instance) {
	for {
		select {
		case <-i.done:
			// closing down, stop the goroutine
			return
		case logMsg := <-i.c:
			for _, f := range m.loggers {
				f(logMsg)
			}
		}
	}
}

func (m *manager) flush(i *instance, wg *sync.WaitGroup) {
	var retryCount int

	for {
		select {
		case logMsg := <-i.c:
			retryCount = 0

			for _, f := range m.loggers {
				f(logMsg)
			}
		default:
			if retryCount >= flushWaitAttempts {
				wg.Done()

				return
			}

			// the log messages may need a tiny bit more time to get slurped up in the channel for
			// each individual instance, so we sleep and count retries to delay things a tick
			time.Sleep(flushWaitSleep)

			retryCount++
		}
	}
}

func (m *manager) panic(f string) {
	printLog(m.formatter(m.instances[panicInstance], Fatal, f))
}

func (m *manager) SetLoggerFormatterAllInstances(formatter Formatter) {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	for _, i := range m.instances {
		i.formatter = formatter
	}
}

func (m *manager) SetLoggerFormatter(name string, formatter Formatter) error {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	i, exists := m.instances[name]
	if !exists {
		return fmt.Errorf("%w: logger '%s' does not exist", ErrLoggingInstance, name)
	}

	i.formatter = formatter

	return nil
}

func (m *manager) SetLoggerIOWriterFormatter(name string, formatter Formatter) error {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	i, exists := m.instances[name]
	if !exists {
		return fmt.Errorf("%w: logger '%s' does not exist", ErrLoggingInstance, name)
	}

	i.ioWriterFormatter = formatter

	return nil
}

func (m *manager) SetLoggerLevelAllInstances(level LogLevel) {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	for _, i := range m.instances {
		i.level = level
	}
}

func (m *manager) SetLoggerLevel(name string, level LogLevel) error {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	i, exists := m.instances[name]
	if !exists {
		return fmt.Errorf("%w: logger '%s' does not exist", ErrLoggingInstance, name)
	}

	i.level = level

	return nil
}

func (m *manager) RegisterLogger(name string, level LogLevel) error {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	_, exists := m.instances[name]
	if exists {
		return fmt.Errorf("%w: logger '%s' already exists", ErrLoggingInstance, name)
	}

	m.instances[name] = &instance{
		lock:      sync.Mutex{},
		name:      name,
		level:     level,
		formatter: m.formatter,
		c:         make(chan string),
		done:      make(chan interface{}),
	}

	go m.start(m.instances[name])

	return nil
}

func (m *manager) RegisterAndGetLogger(name string, level LogLevel) (Instance, error) {
	err := m.RegisterLogger(name, level)
	if err != nil {
		return nil, err
	}

	i, err := m.GetLogger(name)
	if err != nil {
		return nil, err
	}

	return i, nil
}

func (m *manager) MustRegisterAndGetLogger(name string, level LogLevel) Instance {
	i, err := m.RegisterAndGetLogger(name, level)
	if err != nil {
		Panic(err.Error())
	}

	return i
}

func (m *manager) GetLogger(name string) (Instance, error) {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	i, exists := m.instances[name]
	if !exists {
		return nil, fmt.Errorf(
			"%w: logger '%s' does not exist",
			ErrLoggingInstance,
			name,
		)
	}

	return i, nil
}

func (m *manager) DeleteLogger(name string) {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	delete(m.instances, name)
}

// Flush stops all loggers and emits all remaining messages.
func (m *manager) Flush() {
	m.instancesLock.Lock()
	defer m.instancesLock.Unlock()

	wg := &sync.WaitGroup{}

	for _, i := range m.instances {
		wg.Add(1)

		m.flush(i, wg)

		close(i.done)
	}

	wg.Wait()
}
